# RETROFIT: LA PLAGEA #
## DEPENDENCIAS ##
```
compile 'com.squareup.retrofit2:retrofit:2.3.0'
compile 'com.squareup.retrofit2:converter-gson:2.3.0'
```

## PERMISO ##
```
<uses-permission android:name="android.permission.INTERNET"/>
```

## SERVICIO COMO INTERFAZ ##
```
public interface ServiceContacto {
    @POST("create")
    Call<ResponseBody> createContactoBody(@Body Contacto contacto);

    @POST("create")
    @FormUrlEncoded
    Call<ResponseBody> createContactoFields(@Field("Nombre") String nombre, @Field("Telefono") String telefono, @Field("Email") String email);

    @GET("index")
    Call<List<Contacto>> listContacto(@Query("name") String name);

    @GET("index/{id}")
    Call<Contacto> getContacto(@Path("id") int id);
}
```

## PROVEEDOR DEL SERVICIO ##
```
public class ServiceProvider {
    private static ServiceContacto service;

    public static ServiceContacto getService()
    {
        if(service==null){
            service = new Retrofit.Builder()
                    .baseUrl("http://192.168.0.4/foro/contacto/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(ServiceContacto.class);
        }

        return service;
    }
}
```

## USO EN ACTIVIDAD ##
```
private ServiceContacto service;
service = ServiceProveedor.getService();

service.createContacto("luchito","1234","luch@ito").enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String rpta = response.message();

                String msg;
                if(response.isSuccessful()){
                    msg = "Creado correctamente";
                }else{
                    msg = "Error al crear: ";
                }
                Toast.makeText(getBaseContext(),msg+"\n"+rpta,Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getBaseContext(),t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
```