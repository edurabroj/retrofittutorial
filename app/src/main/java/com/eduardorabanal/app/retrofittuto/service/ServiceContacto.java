package com.eduardorabanal.app.retrofittuto.service;

import com.eduardorabanal.app.retrofittuto.models.Contacto;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by USER on 03/12/2017.
 */
public interface ServiceContacto {
    @POST("create")
    Call<ResponseBody> createContactoBody(@Body Contacto contacto);

    @POST("create")
    @FormUrlEncoded
    Call<ResponseBody> createContactoFields(@Field("Nombre") String nombre, @Field("Telefono") String telefono, @Field("Email") String email);

    @GET("index")
    Call<List<Contacto>> listContacto(@Query("name") String name);

    @GET("index/{id}")
    Call<Contacto> getContacto(@Path("id") int id);
}
