package com.eduardorabanal.app.retrofittuto.service;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by USER on 03/12/2017.
 */
public class ServiceProveedor {
    private static ServiceContacto service;

    public static ServiceContacto getService()
    {
        if(service==null){
            service = new Retrofit.Builder()
                    .baseUrl("http://192.168.0.4/foro/contacto/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(ServiceContacto.class);
        }

        return service;
    }
}
