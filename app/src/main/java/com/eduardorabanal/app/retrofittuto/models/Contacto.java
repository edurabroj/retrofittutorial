package com.eduardorabanal.app.retrofittuto.models;

/**
 * Created by USER on 02/12/2017.
 */
public class Contacto {
    private int ContactoId;
    private String Nombre, Telefono, Email;

    @Override
    public String toString() {
        return ContactoId +
                "," + Nombre +
                ", " + Telefono +
                ", " + Email + "\n";
    }

    public Contacto(String nombre, String telefono, String email) {
        Nombre = nombre;
        Telefono = telefono;
        Email = email;
    }

    public int getContactoId() {
        return ContactoId;
    }

    public void setContactoId(int contactoId) {
        ContactoId = contactoId;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String telefono) {
        Telefono = telefono;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }
}
