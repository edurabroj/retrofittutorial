package com.eduardorabanal.app.retrofittuto;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.eduardorabanal.app.retrofittuto.models.Contacto;
import com.eduardorabanal.app.retrofittuto.service.ServiceContacto;
import com.eduardorabanal.app.retrofittuto.service.ServiceProveedor;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private ServiceContacto service;

    private TextView text;
    private EditText queryName,pathId;
    private Button btnListar, btnGetById, btnCreate, btnCreateFields;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        service = ServiceProveedor.getService();

        text = (TextView) findViewById(R.id.text);
        queryName = (EditText) findViewById(R.id.queryName);
        pathId = (EditText) findViewById(R.id.pathId);
        btnListar = (Button) findViewById(R.id.btnListar);
        btnGetById = (Button) findViewById(R.id.btnGetById);
        btnCreate = (Button) findViewById(R.id.btnCreate);
        btnCreateFields = (Button) findViewById(R.id.btnCreateFields);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);

                service.listContacto(queryName.getText().toString()).enqueue(new Callback<List<Contacto>>() {
                    @Override
                    public void onResponse(Call<List<Contacto>> call, Response<List<Contacto>> response) {
                        if(response.isSuccessful()){
                            List<Contacto> lista = response.body();
                            String data="";
                            for(Contacto c : lista){
                                data += c.toString();
                            }
                            text.setText(data);
                        }else{
                            text.setText(response.message());
                        }
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onFailure(Call<List<Contacto>> call, Throwable t) {
                        text.setText(t.getMessage());
                        progressBar.setVisibility(View.GONE);
                    }
                });
            }
        });

        btnGetById.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);

                service.getContacto( Integer.parseInt(pathId.getText().toString()) ).enqueue(new Callback<Contacto>() {
                    @Override
                    public void onResponse(Call<Contacto> call, Response<Contacto> response) {
                        if(response.isSuccessful()){
                            Contacto contacto = response.body();
                            String data = contacto.toString();
                            text.setText(data);
                        }else{
                            text.setText(response.message());
                        }
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onFailure(Call<Contacto> call, Throwable t) {
                        text.setText(t.getMessage());
                        progressBar.setVisibility(View.GONE);
                    }
                });
            }
        });

        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);

                Contacto contact = new Contacto("Mimo","123","mi@mo.com");
                service.createContactoBody(contact).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        String rpta = response.message();
                        if(response.isSuccessful())
                        {
                            //rpta correcta
                            text.setText(rpta);
                        }else{
                            //codigo de error HTTP
                            Toast.makeText(getApplicationContext(),rpta,Toast.LENGTH_SHORT).show();
                        }
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        text.setText(t.getMessage());
                        progressBar.setVisibility(View.GONE);
                    }
                });
            }
        });

        btnCreateFields.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);

                service.createContactoFields("roberta","321","rob@erta.com").enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        String rpta = response.message();
                        if(response.isSuccessful())
                        {
                            text.setText(rpta);
                        }else{
                            Toast.makeText(getApplicationContext(),rpta,Toast.LENGTH_SHORT).show();
                        }
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        text.setText(t.getMessage());
                        progressBar.setVisibility(View.GONE);
                    }
                });
            }
        });
    }
}
